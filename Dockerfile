# Partimos de una base oficial de python
FROM python:3.10
  RUN mkdir /proyectoX
# El directorio de trabajo es desde donde se ejecuta el contenedor al iniciarse
 WORKDIR /proyectoX

# Copiamos todos los archivos del build context al directorio /app del contenedor
 ADD . /proyectoX

# Actualizo el pip
RUN pip install --upgrade pip

# Ejecutamos pip para instalar las dependencias en el contenedor
RUN pip install -r requirements.txt

ADD docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod a+x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]




