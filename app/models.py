from django.db import models

# Create your models here.
# usuario (nombre, apellido, username, email, password, image, status, fecha_creacion)
# categoria (nombre)
# un post tiene usuario y categoria
# post (titulo resumen contenido, imagen, estado) - 1 a muchos con comentarios
# comentario ( nombre, comentario, email, creado_fecha, estado)


class Usuario(models.Model):
    
    id_usuario = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30)
    apellido = models.CharField(max_length=30)
    username = models.CharField(max_length=30)
    email = models.EmailField(max_length=40)
    password = models.CharField(max_length=32)
    image = models.ImageField(upload_to ='uploads/')
    STATUS_CHOICES = [
        ('Activo', 'Activo'),
        ('Inactivo', 'Inactivo'),
    ]
    status = models.CharField(
        max_length=20,
        choices = STATUS_CHOICES,
        default='Borrador',
        )
    fecha_nacimiento = models.DateField()
    def __str__(self):
        texto = '{} {}'.format(
            self.nombre,
            self.apellido,
        )
        return texto

class Categoria(models.Model):
    
    id_categoria = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30)
    Post = models.ManyToManyField(Usuario, through='Post')

    def __str__(self):
        texto = '{}'.format(
            self.nombre,
        )
        return texto

class Post(models.Model):
    id_post = models.AutoField(primary_key=True)
    titulo = models.CharField(max_length=30)
    resumen = models.CharField(max_length=50)
    contenido = models.TextField(max_length=100)
    image = models.ImageField(upload_to ='uploads/')
    STATUS_CHOICES = [
        ('Publicado', 'Publicado'),
        ('Borrador', 'Borrador'),
    ]
    status = models.CharField(
        max_length=20,
        choices = STATUS_CHOICES,
        default='Borrador',
        )
    id_usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE, null=True)
    id_categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, null=True)

    def __str__(self):
        texto = '{}'.format(
            self.titulo,
        )
        return texto

class Comentario(models.Model):
    
    id_comentario = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30)
    comentario = models.TextField()
    email = models.EmailField(max_length=40)
    STATUS_CHOICES = [
        ('Publicado', 'Publicado'),
        ('Borrador', 'Borrador'),
    ]
    status = models.CharField(
        max_length=20,
        choices = STATUS_CHOICES,
        default='Borrador',
        )
    fecha_creado = models.DateField()
    id_post = models.ForeignKey(Post, on_delete=models.CASCADE, null=True)

    def __str__(self):
        texto = '{} {}'.format(
            self.nombre,
            self.email,
        )
        return texto