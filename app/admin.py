from django.contrib import admin

# Importo los modelos
from app.models import Usuario, Comentario, Categoria, Post

# Register your models here.
admin.site.register(Usuario)

admin.site.register(Categoria)
admin.site.register(Comentario)

class ComentarioInline(admin.TabularInline):
    model = Comentario

class PostAdmin(admin.ModelAdmin):
    inlines = [
        ComentarioInline,
    ]

admin.site.register(Post, PostAdmin)